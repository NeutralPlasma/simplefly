package com.neutralplasma.simplefly;

import com.neutralplasma.simplefly.command.MainTabComplete;
import com.neutralplasma.simplefly.command.commands.Fly;
import com.neutralplasma.simplefly.command.commands.FlyManage;
import com.neutralplasma.simplefly.eventHandlers.JoinEvent;
import com.neutralplasma.simplefly.eventHandlers.LeaveEvent;
import com.neutralplasma.simplefly.eventHandlers.OnClickEvent;
import com.neutralplasma.simplefly.fileManagers.FileManager;
import com.neutralplasma.simplefly.fileManagers.PlayerManager;
import com.neutralplasma.simplefly.flyManager.FlyManager;
import com.neutralplasma.simplefly.inventoryHandler.EditMenu;
import com.neutralplasma.simplefly.utils.MainUtil;
import com.neutralplasma.simplefly.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.awt.*;

public class SimpleFly extends JavaPlugin {

    private static SimpleFly INSTANCE = null;
    private static FlyManager flyManager;
    private static FileManager fileManager;
    private static MainTabComplete mainTabComplete;
    private EditMenu editMenu;
    private PlayerManager playerManager;
    private PlayerUtil playerUtil;
    private static boolean debug;


    @Override
    public void onEnable() {
        startupMessage();
        INSTANCE = this;
        fileManager = new FileManager();
        playerManager = new PlayerManager();
        playerUtil = new PlayerUtil();
        flyManager = new FlyManager(playerUtil);
        mainTabComplete = new MainTabComplete();
        editMenu = new EditMenu(this);

        setupConfig();
        setupPlayers();

        //setup messages
        fileManager.setup();
        //Event Handlers
        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new JoinEvent(playerUtil), this);
        pluginManager.registerEvents(new LeaveEvent(), this);
        pluginManager.registerEvents(new OnClickEvent(), this);
        // Main Command
        setupCommands();
        flyManager.startFlyUpdater();
        Bukkit.getConsoleSender().sendMessage(MainUtil.ColorFormat("&a===================================="));
    }

    @Override
    public void onDisable() {
        for(Player player : Bukkit.getOnlinePlayers()){
            flyManager.savePlayer(player);
        }
        disableMessage();
    }


    public static SimpleFly getINSTANCE() {
        return INSTANCE;
    }

    public void setupCommands(){
        getCommand("fly").setExecutor(new Fly());
        getCommand("fly").setTabCompleter(mainTabComplete);
        getCommand("flymanager").setExecutor(new FlyManage(editMenu));
    }

    //Setup UserData
    public void setupPlayers(){
        new BukkitRunnable(){
            @Override
            public void run() {
                for (OfflinePlayer oplayer : Bukkit.getOfflinePlayers()) {
                    playerUtil.setupOfflinePlayer(oplayer);
                }
            }
        }.runTaskAsynchronously(this);
    }

    public void setupConfig(){
        this.saveDefaultConfig();
    }

    public void startupMessage(){
        ConsoleCommandSender sender = Bukkit.getConsoleSender();
        String line = "&a============ &6[ Startup ] &a============";
        String pluginname = "&fPlugin = &6" + this.getName();
        String version = "&fVersion = &6" + this.getDescription().getVersion();
        String action = "&fAction = &aStarting...";
        sender.sendMessage(MainUtil.ColorFormat(line));
        sender.sendMessage(MainUtil.ColorFormat(pluginname));
        sender.sendMessage(MainUtil.ColorFormat(version));
        sender.sendMessage(MainUtil.ColorFormat(action));
    }

    public void disableMessage(){

        ConsoleCommandSender sender = Bukkit.getConsoleSender();
        String line = "&a============ &6[ Turn off ] &a============";
        String pluginname = "&fPlugin = &6" + this.getName();
        String version = "&fVersion = &6" + this.getDescription().getVersion();
        String action = "&fAction = &cDisabling...";
        sender.sendMessage(MainUtil.ColorFormat(line));
        sender.sendMessage(MainUtil.ColorFormat(pluginname));
        sender.sendMessage(MainUtil.ColorFormat(version));
        sender.sendMessage(MainUtil.ColorFormat(action));
        sender.sendMessage(MainUtil.ColorFormat(line));
    }
}
