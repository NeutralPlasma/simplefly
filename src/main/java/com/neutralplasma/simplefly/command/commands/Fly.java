package com.neutralplasma.simplefly.command.commands;

import com.neutralplasma.simplefly.fileManagers.FileManager;
import com.neutralplasma.simplefly.fileManagers.PlayerManager;
import com.neutralplasma.simplefly.flyManager.FlyManager;
import com.neutralplasma.simplefly.utils.MainUtil;
import com.neutralplasma.simplefly.utils.PermissionUtil;
import com.neutralplasma.simplefly.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Fly implements CommandExecutor {

    private String timerPermission = "simplefly.fly.timer";
    private String checkPermission = "simplefly.fly.check";
    private String otherFlyPermission = "simplefly.fly.other";
    private String selfFlyPermission = "simplefly.fly.self";

    public Fly() {

    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) { // if sender is console
            String subcommand = checkSubCommands(args);
            if (subcommand.equalsIgnoreCase("noplayer")) { //checks if sender included player or no
                sender.sendMessage(MainUtil.ColorFormat(FileManager.getInstance().getMessages().getString("unknownPlayer")));
            }
            if (subcommand.equalsIgnoreCase("update")) { // if command returns update
                if (Bukkit.getPlayer(args[0]) == null) { // if player sender provided is null
                    sender.sendMessage(MainUtil.ColorFormat(FileManager.getInstance().getMessages().getString("unknownPlayer")));
                } else { // if provided player is not null
                    Player player = Bukkit.getPlayer(args[0]);
                    updateFlyCommand(player, sender);
                }
            }
            if (subcommand.equalsIgnoreCase("add")) {
                Player target = Bukkit.getPlayer(args[0]);
                if (target != null) {
                    long formatedTime = MainUtil.formatTimeLong(args);
                    long oldtime = FlyManager.getInstance().getPlayerFly(target);
                    formatedTime = oldtime + formatedTime;
                    updateFlyTimer(target, formatedTime, sender, oldtime);
                } else {
                    sender.sendMessage(MainUtil.ColorFormat(FileManager.getInstance().getMessages().getString("unknownPlayer")));
                }

            }

            if (subcommand.equalsIgnoreCase("remove")) {
                Player target = Bukkit.getPlayer(args[0]);
                if (target != null) {
                    long oldtime = FlyManager.getInstance().getPlayerFly(target);
                    long formatedTime = MainUtil.formatTimeLong(args);
                    formatedTime = oldtime - formatedTime;
                    if (formatedTime < 0) {
                        formatedTime = 0L;
                    }
                    updateFlyTimer(target, formatedTime, sender, oldtime);
                } else {
                    sender.sendMessage(MainUtil.ColorFormat(FileManager.getInstance().getMessages().getString("unknownPlayer")));
                }
            }
        } else { // if sender is player
            String subcommand = checkSubCommands(args);
            if (subcommand.equalsIgnoreCase("noplayer")) {
                Player player = (Player) sender;
                updateFlyCommand(player, sender);
            }
            if (subcommand.equalsIgnoreCase("update")) {
                Player target = Bukkit.getPlayer(args[0]);
                if (target != null) {
                    updateFlyCommand(target, sender);
                } else {

                }
            }
            if (subcommand.equalsIgnoreCase("add")) {
                Player target = Bukkit.getPlayer(args[0]);
                if (target != null) {
                    addCommand(target, ((Player) sender).getPlayer(), args);
                }

            }

            if (subcommand.equalsIgnoreCase("remove")) {
                Player target = Bukkit.getPlayer(args[0]);
                if (target != null) {
                    removeCommand(target, ((Player) sender).getPlayer(), args);
                }
            }
            if (subcommand.equalsIgnoreCase("check")) {
                Player target = Bukkit.getPlayer(args[0]);
                if (target != null) {
                    checkCommand(target, (Player) sender, args);
                }
            }
            if (subcommand.equalsIgnoreCase("unknownplayer") && PermissionUtil.hasPermission(otherFlyPermission, (Player) sender)) {
                sender.sendMessage(MainUtil.ColorFormat(FileManager.getInstance().getMessages().getString("unknownPlayer")));
            }
        }

        return true;
    }





    // SubCommands:
    public void addCommand(Player player,Player sender, String[] args){
        //sender has permission to update others fly timer
        if(PermissionUtil.hasPermission(timerPermission, sender)){
            long formatedTime = MainUtil.formatTimeLong(args);
            long oldtime = FlyManager.getInstance().getPlayerFly(player);
            formatedTime = oldtime + formatedTime;
            updateFlyTimer(player, formatedTime, sender, oldtime);
        }
    }

    public void removeCommand(Player player,Player sender, String[] args){
        //sender has permission to update others fly timer
        if(PermissionUtil.hasPermission(timerPermission, sender)){
            long formatedTime = MainUtil.formatTimeLong(args);
            long oldtime = FlyManager.getInstance().getPlayerFly(player);
            formatedTime = oldtime - formatedTime;
            if(formatedTime < 0){
                formatedTime = 0L;
            }
            updateFlyTimer(player, formatedTime, sender, oldtime);
        }
    }


    public void checkCommand(Player target,Player sender, String[] args){
        if(PermissionUtil.hasPermission(checkPermission, sender)){
            long targetfly = FlyManager.getInstance().getPlayerFly(target);
            String formatedTargetFly = " ";
            formatedTargetFly = MainUtil.timerFormat(targetfly, false);
            for(int index = 0; index < args.length; index++){
                String arg = args[index];
                if(index == 2) {
                    if (arg.equalsIgnoreCase("false")) {
                        formatedTargetFly = MainUtil.timerFormat(targetfly, false);
                    }
                    if (arg.equalsIgnoreCase("true")) {
                        formatedTargetFly = MainUtil.timerFormat(targetfly, true);
                    }
                }
            }
            String message = FileManager.getInstance().getMessages().getString("flyRelated.checkCommand");
            message = message.replace("{0}", target.getName()).replace("{1}", formatedTargetFly);
            sender.sendMessage(MainUtil.ColorFormat(message));
        }
    }

    public void updateFlyCommand(Player player, CommandSender sender){

        //target is the sender of command
        if(sender == player){

            if(PermissionUtil.hasPermission(selfFlyPermission, player)) {
                PlayerUtil.getInstance().updateFly(player);
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.updatedSelfFly");
                unformated = unformated.replace("{0}", PlayerUtil.getInstance().getFlyState(player));
                sender.sendMessage(MainUtil.ColorFormat(unformated));
            }
        }else {
            //sender has permission to update fly mode of others.
            if (sender instanceof Player) {
                if (PermissionUtil.hasPermission(otherFlyPermission, player)) { // checks if sender has permission to update others fly
                    PlayerUtil.getInstance().updateFlyState(player, sender); // updates fly
                }
            } else {
                PlayerUtil.getInstance().updateFlyState(player, sender); // updates fly
            }
        }

    }

    // SubCommand check:

    public String checkSubCommands(String[] args){

        if(args.length >= 1){
            //no player provided
            Player player = Bukkit.getPlayer(args[0]);
            if(player == null){
                return "unknownplayer";
            }
            //checks the subcommand stuff.
            if(args.length == 1){
                return "update";
            }
            // contains more than player.
            if(args[1].equalsIgnoreCase("add")){
                return "add";
            }
            if(args[1].equalsIgnoreCase("remove")){
                return "remove";
            }
            if(args[1].equalsIgnoreCase("check")){
                return "check";
            }

        }else {
            return "noplayer";
        }
        return null;
    }
    //Update targets fly timer.
    public void updateFlyTimer(Player target, Long time, CommandSender sender, Long oldTime) {
        FlyManager.getInstance().setTime(target, time);
        // Message stuff from here on:
        String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
        unformated = unformated.replace("{0}", target.getName()); // Replaces first placeholder with targets name.
        unformated = unformated.replace("{1}", MainUtil.timerFormat(time, false)); // replaces placeholder with new time.
        unformated = unformated.replace("{2}", MainUtil.timerFormat(oldTime, false)); // replaces placeholder with old time.
        sender.sendMessage(MainUtil.ColorFormat(unformated)); // sends message.
    }

}
