package com.neutralplasma.simplefly.command.commands;

import com.neutralplasma.simplefly.fileManagers.FileManager;
import com.neutralplasma.simplefly.inventoryHandler.EditMenu;
import com.neutralplasma.simplefly.utils.MainUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FlyManage implements CommandExecutor {
    private EditMenu editMenu;

    public FlyManage(EditMenu editMenu){
        this.editMenu = editMenu;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(sender instanceof Player){
            Player player = (Player) sender;
            if(player.hasPermission("simplefly.command.flymanager")){
                if(args.length >= 1){
                    Player target = Bukkit.getPlayer(args[0]);
                    if(target != null){
                        editMenu.openInventory(player, target);
                    }else{
                        player.sendMessage(MainUtil.ColorFormat(FileManager.getInstance().getMessages().getString("unknownPlayer")));
                    }
                }else{
                    player.sendMessage(MainUtil.ColorFormat(FileManager.getInstance().getMessages().getString("unknownPlayer")));
                }
            }else{
                MainUtil.noPermissionMessage(sender, "simplefly.command.flymanager");
            }
        }
        return true;
    }

}
