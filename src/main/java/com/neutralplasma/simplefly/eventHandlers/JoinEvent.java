package com.neutralplasma.simplefly.eventHandlers;

import com.neutralplasma.simplefly.SimpleFly;
import com.neutralplasma.simplefly.fileManagers.PlayerManager;
import com.neutralplasma.simplefly.flyManager.FlyManager;
import com.neutralplasma.simplefly.utils.PlayerUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;


public class JoinEvent implements Listener {
    private PlayerUtil playerUtil;
    private PlayerManager playerManager;

    public JoinEvent(PlayerUtil playerUtil){
        this.playerUtil = playerUtil;
        this.playerManager = PlayerManager.getInstance();
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();
        String ipAddress = player.getAddress().getAddress().toString();
        FlyManager.getInstance().addPlayer(player.getUniqueId());
        if(FlyManager.getInstance().getPlayerFly(player) > 0){
            if(SimpleFly.getINSTANCE().getConfig().getBoolean("commands.fly.autoEnableOnJoin") && player.hasPermission("simplefly.fly") && player.hasPermission("simplefly.safejoin")) {
                player.setAllowFlight(true);
                player.setFlying(true);
            }else{
                player.setAllowFlight(false);
                player.setFlying(false);
            }
        }
    }
}
