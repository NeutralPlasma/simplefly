package com.neutralplasma.simplefly.eventHandlers;

import com.neutralplasma.simplefly.flyManager.FlyManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class LeaveEvent implements Listener {
    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event){
        FlyManager.getInstance().savePlayer(event.getPlayer());
    }
}
