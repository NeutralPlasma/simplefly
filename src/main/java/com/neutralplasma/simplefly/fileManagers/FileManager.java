package com.neutralplasma.simplefly.fileManagers;

import com.neutralplasma.simplefly.SimpleFly;
import com.neutralplasma.simplefly.utils.MainUtil;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;

public class FileManager {
    private Plugin plugin = SimpleFly.getINSTANCE();
    private static FileManager INSTANCE;
    public FileConfiguration messagesconfiguration;
    public File messagesFile;

    public FileManager(){
        INSTANCE = this;
    }

    public void setup(){

        //creates plugin folder
        if(!plugin.getDataFolder().exists()){
            plugin.getDataFolder().mkdir();
        }
        //---------------------

        messagesFile = new File(plugin.getDataFolder(), "messages.yml");
        if(!messagesFile.exists()){
            try{
                messagesFile.createNewFile();
                messagesconfiguration = YamlConfiguration.loadConfiguration(messagesFile);
                plugin.saveResource("messages.yml", true);
                Bukkit.getConsoleSender().sendMessage( MainUtil.ColorFormat("&aSuccessfully created messages.yml file!"));


            }catch (IOException e){
                Bukkit.getConsoleSender().sendMessage(MainUtil.ColorFormat("&cFailed to create messages.yml file, Error: &f" + e.getMessage()));

            }

        }
        messagesconfiguration = YamlConfiguration.loadConfiguration(messagesFile);
    }

    public FileConfiguration getMessages() {
        return messagesconfiguration;
    }


    public void saveMessages(){
        try{
            messagesconfiguration.save(messagesFile);
            Bukkit.getConsoleSender().sendMessage(MainUtil.ColorFormat("&aSuccessfully saved messages.yml file."));
        }catch(IOException e){
            Bukkit.getConsoleSender().sendMessage(MainUtil.ColorFormat("&cFailed to save messages.yml file, Error: &f" + e.getMessage()));
        }
    }

    public void reloadMessages() {
        messagesconfiguration = YamlConfiguration.loadConfiguration(messagesFile);
        Bukkit.getConsoleSender().sendMessage(MainUtil.ColorFormat("&aReloaded messages.yml file."));
    }

    public static FileManager getInstance(){
        return INSTANCE;
    }


}
