package com.neutralplasma.simplefly.flyManager;

import com.neutralplasma.simplefly.SimpleFly;
import com.neutralplasma.simplefly.fileManagers.FileManager;
import com.neutralplasma.simplefly.fileManagers.PlayerManager;
import com.neutralplasma.simplefly.utils.ActionBarMessage;
import com.neutralplasma.simplefly.utils.GetPotion;
import com.neutralplasma.simplefly.utils.PlayerUtil;
import com.neutralplasma.simplefly.utils.MainUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.UUID;

public class FlyManager {
    private HashMap<UUID, Long> players = new HashMap<>();
    private PlayerUtil playerUtil;
    private String flyTimerType;
    private static FlyManager INSTANCE = null;
    private SimpleFly flyTimer;

    public FlyManager(PlayerUtil playerUtil) {
        this.playerUtil = playerUtil;
        INSTANCE = this;
        this.flyTimer = SimpleFly.getINSTANCE();
    }

    public void startFlyUpdater() {
        new BukkitRunnable() {
            @Override
            public void run() {
                for (UUID playerUuid : players.keySet()) {
                    Player player = Bukkit.getPlayer(playerUuid);
                    if (!(permissionChecks(player))) {
                        if (flyCheck(player)) {
                            long flyTime = players.get(playerUuid);
                            if(flyTime < 0){
                                flyTime = 0;
                            }
                            if (flyTime >= 0) {
                                flyTime--;
                                if(flyTime < 0){
                                    flyTime = 0;
                                }
                                if (flyTime <= flyTimer.getConfig().getInt("commands.fly.flyTimerRunOutStartAt")) {
                                    String message = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerCountDown");
                                    message = message.replace("{0}", MainUtil.colorFormatTime(flyTime));
                                    player.sendMessage(MainUtil.ColorFormat(message));
                                }
                                if(flyTime == 2){
                                    PotionEffect potionEffect = GetPotion.getPotion("DAMAGE_RESISTANCE", 11
                                            , flyTimer.getConfig().getInt("commands.fly.flyTimerPotionDuration")
                                            , flyTimer.getConfig().getInt("commands.fly.flyTimerPotionStrength"));

                                    player.addPotionEffect(potionEffect, true);
                                }
                                if (checkVersion()) {
                                    sendActionBar(player, flyTime);
                                }
                            }
                            if (flyTime <= 0) {
                                player.setAllowFlight(false);
                            }
                            players.put(playerUuid, flyTime);
                        }
                    }
                }
            }
        }.runTaskTimer(SimpleFly.getINSTANCE(), 0L, 20L);
    }

    public void addPlayer(UUID uuid) {
        long timer = PlayerManager.getInstance().getFile(uuid.toString()).getLong("flyTimer");
        players.put(uuid, timer);
    }

    public void savePlayer(Player player) {
        long timer = players.get(player.getUniqueId());
        playerUtil.updatePlayerDataLong(player, "flyTimer", timer);
        players.remove(player.getUniqueId());
    }

    public long getPlayerFly(Player player) {
        UUID uuid = player.getUniqueId();
        long timer = players.get(uuid);
        return timer;
    }

    public boolean permissionChecks(Player player) {
        if (player.hasPermission("simplefly.fly.ignoretimer")) {
            return true;
        }

        return false;
    }

    public boolean checkVersion() {
        String version = Bukkit.getVersion();
        if (version.contains("1.8")) {
            return false;
        }
        return true;
    }

    public void sendActionBar(Player player, long timer) {
        if (player.hasPermission("simplefly.fly.actionbar")) {
            String message = FileManager.getInstance().getMessages().getString("flyRelated.actionBarFlyTimer");
            message = message.replace("{0}", MainUtil.timerFormat(timer, true));
            message = MainUtil.ColorFormat(message);
            ActionBarMessage.sendActionBarMessage(player, message);
        }
    }

    public boolean flyCheck(Player player) {
        flyTimerType = flyTimer.getConfig().getString("commands.fly.flyTimerType");
        if (flyTimerType.equalsIgnoreCase("ground")) {
            if (player.getAllowFlight()) {
                return true;
            }
            return false;
        } else if (flyTimerType.equalsIgnoreCase("air")) {
            if (player.isFlying()) {
                return true;
            }
            return false;
        }
        return false;
    }

    public boolean setTime(Player player, long timer){
        try {
            if(timer < 0){
                timer = 0;
            }
            players.put(player.getUniqueId(), timer);
            return true;
        }catch (Exception error){
            return false;
        }
    }

    public boolean addTime(Player player, long timer){
        try{
            long currentime = getPlayerFly(player);
            timer += currentime;
            if(timer < 0){
                timer = 0;
            }
            players.put(player.getUniqueId(), timer);
            return true;
        }catch (Exception error){
            return false;
        }
    }

    public static FlyManager getInstance(){
        return INSTANCE;
    }

}
