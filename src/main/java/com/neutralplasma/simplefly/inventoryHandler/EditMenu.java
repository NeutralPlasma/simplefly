package com.neutralplasma.simplefly.inventoryHandler;

import com.neutralplasma.simplefly.SimpleFly;
import com.neutralplasma.simplefly.command.commands.FlyManage;
import com.neutralplasma.simplefly.fileManagers.FileManager;
import com.neutralplasma.simplefly.flyManager.FlyManager;
import com.neutralplasma.simplefly.inventoryHandler.actions.*;
import com.neutralplasma.simplefly.utils.GuiItems;
import com.neutralplasma.simplefly.utils.MainUtil;
import com.neutralplasma.simplefly.utils.PlayerUtil;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class EditMenu {
    private InventoryCreator inventoryCreator;
    private SimpleFly simpleFly;
    List<Integer> items = new ArrayList<>();

    public EditMenu(SimpleFly simpleFly){
        this.simpleFly = simpleFly;

        inventoryCreator = new InventoryCreator(36, "Simple");
    }



    public void openInventory(Player player, Player target){
        inventoryCreator = new InventoryCreator(36, target.getName());
        setupItems(target, player);
        Inventory inventory = inventoryCreator.getInventory();
        player.openInventory(inventory);
    }


    private void setupItems(Player target, Player player){
        Icon background = getBackGround();
        items.clear();
        if(player.hasPermission("simplefly.fly.other")) {
            updateFlyItem(target);
            items.add(35);
        }
        if(player.hasPermission("simplefly.fly.timer")){
            addHours(target);
            addMinutes(target);
            addSeconds(target);
            removeHours(target);
            removeMinutes(target);
            removeSeconds(target);
            items.add(14); // add hours
            items.add(15); // add minutes
            items.add(16); // add seconds
            items.add(12); // remove hours
            items.add(11); // remove minutes
            items.add(10); // remove seconds
        }
        if(player.hasPermission("simplefly.fly")){
            currentTime(target);
            items.add(13); // current time.
        }


        for(int x = 0 ; x < 36; x++){
            if(!items.contains(x)) {
                inventoryCreator.setIcon(x, background);
            }
        }
    }

    public void addHours(Player target){
        ItemStack bookAddHours = new ItemStack(Material.PAPER);
        ItemMeta meta = bookAddHours.getItemMeta();
        List<String> lore = new ArrayList<>();
        meta.setDisplayName(MainUtil.ColorFormat("&8(&bAdd hours&8)"));
        lore.add(MainUtil.ColorFormat("&7Click: &a+1 &7" + FileManager.getInstance().getMessages().getString("global.hour")));
        lore.add(MainUtil.ColorFormat("&7Shift R-Click: &a+5 &7" + FileManager.getInstance().getMessages().getString("global.hours")));
        lore.add(MainUtil.ColorFormat("&7Shift L-Click: &a+10 &7" + FileManager.getInstance().getMessages().getString("global.hours")));
        meta.setLore(lore);
        bookAddHours.setItemMeta(meta);
        Icon addHours = new Icon(bookAddHours).addLeftClickAction(new LeftClickAction() {
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, 3600L);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });
        addHours.addRightClickAction(new RightClickAction() {
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, 3600L);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });

        addHours.addShiftRClickAction(new ShiftRClickAction(){
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, 3600L*5);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });

        addHours.addShiftLClickAction(new ShiftLClickAction() {
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, 3600L*10);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });
        inventoryCreator.setIcon(16, addHours);
    }
    public void addMinutes(Player target){
        ItemStack bookAddMinutes = new ItemStack(Material.PAPER);
        ItemMeta meta = bookAddMinutes.getItemMeta();
        List<String> lore = new ArrayList<>();
        meta.setDisplayName(MainUtil.ColorFormat("&8(&bAdd minutes&8)"));
        lore.add(MainUtil.ColorFormat("&7Click: &a+1 &7" + FileManager.getInstance().getMessages().getString("global.minute")));
        lore.add(MainUtil.ColorFormat("&7Shift R-Click: &a+5 &7" + FileManager.getInstance().getMessages().getString("global.minutes")));
        lore.add(MainUtil.ColorFormat("&7Shift L-Click: &a+10 &7" + FileManager.getInstance().getMessages().getString("global.minutes")));
        meta.setLore(lore);
        bookAddMinutes.setItemMeta(meta);
        Icon addMinutes = new Icon(bookAddMinutes).addLeftClickAction(new LeftClickAction() {
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, 60L);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });
        addMinutes.addRightClickAction(new RightClickAction() {
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, 60L);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });

        addMinutes.addShiftRClickAction(new ShiftRClickAction(){
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, 60L*5);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });

        addMinutes.addShiftLClickAction(new ShiftLClickAction() {
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, 60L*10);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });
        inventoryCreator.setIcon(15, addMinutes);
    }
    public void addSeconds(Player target){
        ItemStack bookAddSeconds = new ItemStack(Material.PAPER);
        ItemMeta meta = bookAddSeconds.getItemMeta();
        List<String> lore = new ArrayList<>();
        meta.setDisplayName(MainUtil.ColorFormat("&8(&bAdd seconds&8)"));
        lore.add(MainUtil.ColorFormat("&7Click: &a+1 &7" + FileManager.getInstance().getMessages().getString("global.second")));
        lore.add(MainUtil.ColorFormat("&7Shift R-Click: &a+5 &7" + FileManager.getInstance().getMessages().getString("global.seconds")));
        lore.add(MainUtil.ColorFormat("&7Shift L-Click: &a+10 &7" + FileManager.getInstance().getMessages().getString("global.seconds")));
        meta.setLore(lore);
        bookAddSeconds.setItemMeta(meta);
        Icon addSeconds = new Icon(bookAddSeconds).addLeftClickAction(new LeftClickAction() {
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, 1L);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });
        addSeconds.addRightClickAction(new RightClickAction() {
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, 1L);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });

        addSeconds.addShiftRClickAction(new ShiftRClickAction(){
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, 1L*5);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });

        addSeconds.addShiftLClickAction(new ShiftLClickAction() {
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, 1L*10);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });
        inventoryCreator.setIcon(14, addSeconds);
    }

    public void removeHours(Player target){
        ItemStack bookRemoveHours = new ItemStack(Material.PAPER);
        ItemMeta meta = bookRemoveHours.getItemMeta();
        List<String> lore = new ArrayList<>();
        meta.setDisplayName(MainUtil.ColorFormat("&8(&cRemove hours&8)"));
        lore.add(MainUtil.ColorFormat("&7Click: &c-1 &7" + FileManager.getInstance().getMessages().getString("global.hour")));
        lore.add(MainUtil.ColorFormat("&7Shift R-Click: &c-5 &7" + FileManager.getInstance().getMessages().getString("global.hours")));
        lore.add(MainUtil.ColorFormat("&7Shift L-Click: &c-10 &7" + FileManager.getInstance().getMessages().getString("global.hours")));
        meta.setLore(lore);
        bookRemoveHours.setItemMeta(meta);
        Icon removeHours = new Icon(bookRemoveHours).addLeftClickAction(new LeftClickAction() {
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, -3600L);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });
        removeHours.addRightClickAction(new RightClickAction() {
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, -3600L);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });

        removeHours.addShiftRClickAction(new ShiftRClickAction(){
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, -3600L*5);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });

        removeHours.addShiftLClickAction(new ShiftLClickAction() {
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, -3600L*10);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });
        inventoryCreator.setIcon(12, removeHours);
    }
    public void removeMinutes(Player target){
        ItemStack bookRemoveMinutes = new ItemStack(Material.PAPER);
        ItemMeta meta = bookRemoveMinutes.getItemMeta();
        List<String> lore = new ArrayList<>();
        meta.setDisplayName(MainUtil.ColorFormat("&8(&cRemove minutes&8)"));
        lore.add(MainUtil.ColorFormat("&7Click: &c-1 &7" + FileManager.getInstance().getMessages().getString("global.minute")));
        lore.add(MainUtil.ColorFormat("&7Shift R-Click: &c-5 &7" + FileManager.getInstance().getMessages().getString("global.minutes")));
        lore.add(MainUtil.ColorFormat("&7Shift L-Click: &c-10 &7" + FileManager.getInstance().getMessages().getString("global.minutes")));
        meta.setLore(lore);
        bookRemoveMinutes.setItemMeta(meta);
        Icon removeMinutes = new Icon(bookRemoveMinutes).addLeftClickAction(new LeftClickAction() {
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, -60L);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });
        removeMinutes.addRightClickAction(new RightClickAction() {
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, -60L);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });

        removeMinutes.addShiftRClickAction(new ShiftRClickAction(){
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, -60L*5);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });

        removeMinutes.addShiftLClickAction(new ShiftLClickAction() {
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, -60L*10);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });
        inventoryCreator.setIcon(11, removeMinutes);
    }
    public void removeSeconds(Player target){
        ItemStack bookRemoveSeconds = new ItemStack(Material.PAPER);
        ItemMeta meta = bookRemoveSeconds.getItemMeta();
        List<String> lore = new ArrayList<>();
        meta.setDisplayName(MainUtil.ColorFormat("&8(&cRemove seconds&8)"));
        lore.add(MainUtil.ColorFormat("&7Click: &c-1 &7" + FileManager.getInstance().getMessages().getString("global.second")));
        lore.add(MainUtil.ColorFormat("&7Shift R-Click: &c-5 &7" + FileManager.getInstance().getMessages().getString("global.seconds")));
        lore.add(MainUtil.ColorFormat("&7Shift L-Click: &c-10 &7" + FileManager.getInstance().getMessages().getString("global.seconds")));
        meta.setLore(lore);
        bookRemoveSeconds.setItemMeta(meta);
        Icon removeSeconds = new Icon(bookRemoveSeconds).addLeftClickAction(new LeftClickAction() {
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, -1L);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });
        removeSeconds.addRightClickAction(new RightClickAction() {
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, -1L);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });

        removeSeconds.addShiftRClickAction(new ShiftRClickAction(){
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, -1L*5);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });

        removeSeconds.addShiftLClickAction(new ShiftLClickAction() {
            @Override
            public void execute(Player player) {
                String unformated = FileManager.getInstance().getMessages().getString("flyRelated.flyTimerUpdate");
                unformated = unformated.replace("{0}", target.getName());
                unformated = unformated.replace("{2}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                FlyManager.getInstance().addTime(target, -1L*10);
                unformated = unformated.replace("{1}", MainUtil.timerFormat(FlyManager.getInstance().getPlayerFly(target), false));
                player.sendMessage(MainUtil.ColorFormat(unformated));
                openInventory(player, target);
            }
        });
        inventoryCreator.setIcon(10, removeSeconds);
    }

    public void updateFlyItem(Player target){
        ItemStack bookFlyState = new ItemStack(Material.BOOK);
        ItemMeta meta = bookFlyState.getItemMeta();
        List<String> lore = new ArrayList<>();
        meta.setDisplayName(MainUtil.ColorFormat("&8(&bFly State&8)"));
        String flyStats = PlayerUtil.getInstance().getFlyState(target);
        final String flystats2 = flyStats;
        if (flyStats.equalsIgnoreCase("enabled")) {
            flyStats = MainUtil.ColorFormat(FileManager.getInstance().getMessages().getString("guiRelated.enabled"));
        }else{
            flyStats = MainUtil.ColorFormat(FileManager.getInstance().getMessages().getString("guiRelated.disabled"));
        }
        lore.add(MainUtil.ColorFormat(FileManager.getInstance().getMessages().getString("guiRelated.flyState")).replace("{0}", flyStats));
        lore.add(MainUtil.ColorFormat("&7Click to update."));
        meta.setLore(lore);
        bookFlyState.setItemMeta(meta);
        Icon protocol = new Icon(bookFlyState).addClickAction(new ClickAction() {
            @Override
            public void execute(Player player) {
                PlayerUtil.getInstance().updateFlyState(target, player);
                openInventory(player, target);
            }
        });
        inventoryCreator.setIcon(35, protocol);
    }

    public void currentTime(Player target){
        ItemStack bookFlytime = new ItemStack(Material.BOOK);
        ItemMeta meta = bookFlytime.getItemMeta();
        List<String> lore = new ArrayList<>();
        meta.setDisplayName(MainUtil.ColorFormat("&8(&bFly Time&8)"));
        long flyStats = FlyManager.getInstance().getPlayerFly(target);
        String flyTime = MainUtil.timerFormat(flyStats, true);
        if(target.hasPermission("simplefly.fly.ignoretimer")){
            flyTime = FileManager.getInstance().getMessages().getString("global.infinity");
        }
        lore.add(MainUtil.ColorFormat(FileManager.getInstance().getMessages().getString("guiRelated.currentTime")).replace("{0}", flyTime));
        meta.setLore(lore);
        bookFlytime.setItemMeta(meta);
        Icon protocol = new Icon(bookFlytime).addClickAction(new ClickAction() {
            @Override
            public void execute(Player player) {
            }
        });
        inventoryCreator.setIcon(13, protocol);
    }

    public Icon getBackGround(){
        ItemStack glass;
        glass = new ItemStack(GuiItems.getGlass(7));
        ItemMeta meta = glass.getItemMeta();
        meta.setDisplayName("");
        glass.setItemMeta(meta);
        Icon background = new Icon(glass);
        background.addClickAction(new ClickAction() {
            @Override
            public void execute(Player player) {
            }
        });
        return background;
    }

}
