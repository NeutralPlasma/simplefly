package com.neutralplasma.simplefly.inventoryHandler.actions;

import org.bukkit.entity.Player;

public interface ClickAction {

    void execute(Player player);

}
