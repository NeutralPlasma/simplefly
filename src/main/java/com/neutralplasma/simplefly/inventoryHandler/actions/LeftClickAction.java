package com.neutralplasma.simplefly.inventoryHandler.actions;

import org.bukkit.entity.Player;

public interface LeftClickAction {

    void execute(Player player);

}
