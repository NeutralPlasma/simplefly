package com.neutralplasma.simplefly.inventoryHandler.actions;

import org.bukkit.entity.Player;

public interface RightClickAction {

    void execute(Player player);

}