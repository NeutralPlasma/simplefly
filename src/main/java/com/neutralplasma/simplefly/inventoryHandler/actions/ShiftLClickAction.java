package com.neutralplasma.simplefly.inventoryHandler.actions;

import org.bukkit.entity.Player;

public interface ShiftLClickAction {
    void execute(Player player);
}
