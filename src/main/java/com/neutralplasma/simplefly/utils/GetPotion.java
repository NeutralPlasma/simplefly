package com.neutralplasma.simplefly.utils;

import org.bukkit.Bukkit;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class GetPotion {


    public static boolean notLegacy(String version){
        if(version.contains("1.8") || version.contains("1.9") || version.contains("1.10") || version.contains("1.11")){
            return false;
        }else{
            return true;
        }
    }

    public static PotionEffect getPotion(String potionEffectType, int id, int duration, int stregth){
        PotionEffectType potionEffectType1;
        if(!notLegacy(Bukkit.getVersion())) {
            potionEffectType1 = PotionEffectType.getByName(potionEffectType);
        }else{
            potionEffectType1 = PotionEffectType.getById(id);
        }
        PotionEffect potionEffect = new PotionEffect(potionEffectType1, duration, stregth);

        return potionEffect;
    }
}
