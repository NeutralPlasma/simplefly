package com.neutralplasma.simplefly.utils;

import com.neutralplasma.simplefly.SimpleFly;
import com.neutralplasma.simplefly.fileManagers.FileManager;
import com.neutralplasma.simplefly.flyManager.FlyManager;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class MainUtil {
    public static String ColorFormat(String text){
       String formated = ChatColor.translateAlternateColorCodes('&', text);
       return formated;
    }
    public static String timerFormat(Long time, boolean formatColor){
        String fullyFormatted;
        DateFormat dateFormat = new SimpleDateFormat("DDD-HH-mm-ss-SSS");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String formattedDate = dateFormat.format(new Date(time*1000L));
        String parts[] = formattedDate.split("-");
        Long days = Long.parseLong(parts[0]);
        days--;
        fullyFormatted = days + "d " + parts[1] + "h " + parts[2] + "m " + parts[3] + "s";
        if(days < 1){
            fullyFormatted = parts[1] + "h " + parts[2] + "m " + parts[3] + "s";
            if(parts[1].equals("00")){
                fullyFormatted = parts[2] + "m " + parts[3] + "s";
                if(parts[2].equals("00")){
                    fullyFormatted = parts[3] + "s";
                }
            }
        }
        if(time <= SimpleFly.getINSTANCE().getConfig().getInt("commands.fly.flyTimerRunOutStartAt") && formatColor){
            fullyFormatted = FileManager.getInstance().getMessages().getString("flyRelated.flyLastSecondsColor") + fullyFormatted;
        }
        return fullyFormatted;
    }

    public static String colorFormatTime(long time){
        if(time <= SimpleFly.getINSTANCE().getConfig().getInt("commands.fly.flyTimerRunOutStartAt")){
            return FileManager.getInstance().getMessages().getString("flyRelated.flyLastSecondsColor") + time;
        }
        return String.valueOf(time);
    }

    public static void noPermissionMessage(CommandSender sender, String commandPermission){
        String unformated = FileManager.getInstance().getMessages().getString("noPermission");
        unformated = unformated.replace("{0}", commandPermission);
        sender.sendMessage(ColorFormat(unformated));
    }

    public static Long formatTimeLong(String[] numbers) {
        Long totalNumber = 0L;
        for (int index = 0; index < numbers.length; index++) {

            String text = numbers[index];
            text = text.toUpperCase();
            if (index >= 2) {
                if (text.contains("Y")) {
                    text = text.replace("Y", "");
                    int rawnumber = Integer.valueOf(text);
                    Long number = rawnumber * Long.valueOf("31536000");
                    totalNumber += number;
                } else if (text.contains("W")) {
                    text = text.replace("W", "");
                    int rawnumber = Integer.valueOf(text);
                    Long number = rawnumber * Long.valueOf("604800");
                    totalNumber += number;
                } else if (text.contains("D")) {
                    text = text.replace("D", "");
                    int rawnumber = Integer.valueOf(text);
                    Long number = rawnumber * Long.valueOf("86400");
                    totalNumber += number;
                } else if (text.contains("H")) {
                    text = text.replace("H", "");
                    int rawnumber = Integer.valueOf(text);
                    Long number = rawnumber * Long.valueOf("3600");
                    totalNumber += number;
                } else if (text.contains("M")) {
                    text = text.replace("M", "");
                    int rawnumber = Integer.valueOf(text);
                    Long number = rawnumber * Long.valueOf("60");
                    totalNumber += number;
                } else if (text.contains("S")) {
                    text = text.replace("S", "");
                    Long rawnumber = Long.valueOf(text);
                    totalNumber += rawnumber;
                }
            }
        }
        return totalNumber;
    }


}
