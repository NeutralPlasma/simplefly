package com.neutralplasma.simplefly.utils;

import com.neutralplasma.simplefly.fileManagers.FileManager;
import com.neutralplasma.simplefly.flyManager.FlyManager;
import org.bukkit.entity.Player;

public class PermissionUtil {

    public static boolean hasPermission(String permission, Player player){
        if(permission.equalsIgnoreCase("simplefly.fly.self")){
            if((player.hasPermission("simplefly.fly") && FlyManager.getInstance().getPlayerFly(player) > 0) || player.hasPermission("simplefly.fly.ignoretimer")){
                return true;
            }
            if(player.hasPermission("simplefly.fly") && FlyManager.getInstance().getPlayerFly(player) <= 0){
                player.sendMessage(
                        MainUtil.ColorFormat(
                                FileManager.getInstance().getMessages().getString("flyRelated.noFlyTimer")
                        )
                );
                return false;
            }
            MainUtil.noPermissionMessage(player, "simplefly.fly");
            return false;
        }

        if(player.hasPermission(permission)){
            return true;
        }
        MainUtil.noPermissionMessage(player, permission);
        return false;
    }
}
