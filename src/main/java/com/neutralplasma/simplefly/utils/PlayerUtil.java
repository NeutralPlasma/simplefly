package com.neutralplasma.simplefly.utils;

import com.neutralplasma.simplefly.SimpleFly;
import com.neutralplasma.simplefly.fileManagers.FileManager;
import com.neutralplasma.simplefly.fileManagers.PlayerManager;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.File;


public class PlayerUtil {
    private PlayerManager pm = PlayerManager.getInstance();
    private static PlayerUtil INSTANCE = null;

    public PlayerUtil(){
        INSTANCE = this;
    }
    public void setupPlayer(Player player){
        String fileName = player.getUniqueId().toString();
        if(!playerExists(player)){
            pm.createFile(fileName);
            pm.getFile(fileName).set("lastNameUsed", player.getName());
            pm.getFile(fileName).set("flyTimer", 0L);
            pm.saveFile(fileName);
        }
    }
    public void setupOfflinePlayer(OfflinePlayer player){
        String fileName = player.getUniqueId().toString();
        if(!offlinePlayerExists(player)){
            pm.createFile(fileName);
            pm.getFile(fileName).set("lastNameUsed", player.getName());
            pm.getFile(fileName).set("flyTimer", 0L);
            pm.saveFile(fileName);
        }
    }
    public void updatePlayerDataString(Player player, String toUpdate, String updateParameters){
        String fileName = player.getUniqueId().toString();
        if(!playerExists(player)){
            setupPlayer(player);
        }
        pm.getFile(fileName).set(toUpdate, updateParameters);
        pm.saveFile(fileName);
    }

    public void updatePlayerDataBoolean(Player player, String toUpdate, Boolean updateParameters){
        String fileName = player.getUniqueId().toString();
        if (!playerExists(player)) {
            setupPlayer(player);
        }
        pm.getFile(fileName).set(toUpdate, updateParameters);
        pm.saveFile(fileName);
    }
    public void updatePlayerDataObject(Player player, String toUpdate, Object updateParameters){
        String fileName = player.getUniqueId().toString();
        if(!playerExists(player)){
            setupPlayer(player);
        }
        pm.getFile(fileName).set(toUpdate, updateParameters);
        pm.saveFile(fileName);
    }
    public void updatePlayerDataLong(Player player, String toUpdate, Long updateParameters){
        String fileName = player.getUniqueId().toString();
        if(!playerExists(player)){
            setupPlayer(player);
        }
        pm.getFile(fileName).set(toUpdate, updateParameters);
        pm.saveFile(fileName);
    }
    public boolean playerExists(Player player){
        String fileName = player.getUniqueId().toString();
        File nfile = new File(SimpleFly.getINSTANCE().getDataFolder() + "/userdata", fileName + ".yml");
        if(!nfile.exists()){
            return false;
        }else{
            return true;
        }
    }
    public boolean offlinePlayerExists(OfflinePlayer player){
        String fileName = player.getUniqueId().toString();
        File nfile = new File(SimpleFly.getINSTANCE().getDataFolder() + "/userdata", fileName + ".yml");
        if(!nfile.exists()){
            return false;
        }else{
            return true;
        }
    }

    public void updateFly(Player target) {
        if (target == null) { // checks if target is null;
            return;
        }
        if (target.getAllowFlight()) { // Checks if player has enabled fly
            target.setAllowFlight(false); // disables players fly.
        } else {
            target.setAllowFlight(true); // enables players fly.
        }
    }

    //check if user has fly enabled or no.
    public String getFlyState(Player target) {
        if (target == null) { // checks if target is null
            return null;
        }
        if (target.getAllowFlight()) { // if player has enabled fly
            return "enabled";
        } else {
            return "disabled";
        }
    }

    public void updateFlyState(Player target, CommandSender sender){
        updateFly(target);
        String unformated = FileManager.getInstance().getMessages().getString("flyRelated.updatedOtherFly");
        unformated = unformated.replace("{0}", getFlyState(target));
        unformated = unformated.replace("{1}", target.getName());
        sender.sendMessage(MainUtil.ColorFormat(unformated));
        String unformated2 = FileManager.getInstance().getMessages().getString("flyRelated.notifyFlyUpdateTarget");
        unformated2 = unformated2.replace("{0}", getFlyState(target));
        target.sendMessage(MainUtil.ColorFormat(unformated2));
    }


    public static PlayerUtil getInstance(){
        return INSTANCE;
    }
}
